package test;

import org.testng.Assert;
import org.testng.annotations.Test;
import Base.BaseTest;
import static io.restassured.RestAssured.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static org.hamcrest.Matchers.*;
import java.util.Map;


public class Test_Get_Request extends BaseTest{
	
		
	@Test 
	public void testCreatPetByPost () {
		
		// add pet
		
		RestAssured.basePath ="pet";
		
		Response res = 
		given().log().all()
		      .header("Authorization", "Bearer "+assessToken)
		      .contentType(ContentType.JSON)
		      .body(pet)
		.when()
		     .post()
		.then()
		     .statusCode(200)                                    //verify status code
		     .assertThat().body("name", equalTo(pet.getName())) // verify created pet successfully
		     .extract().response();
		    
		
		String responseBody = res.asString();
		System.out.println("response body is "+ responseBody);
			
	}
	
	@Test (dependsOnMethods = { "testCreatPetByPost" })
	public void testGetCreatedPet() {
		
		// get pet
		String id = Integer.toString(pet.getId());
		RestAssured.basePath ="pet/"+id;
		
		Response res = 
				
		given()
		 .header("Authorization", "Bearer "+assessToken)
		.when()
		     .get()
		 .then()
		   .statusCode(200)
		   .assertThat().body("name", equalTo(pet.getName()))
		   .extract().response();
			
		// validation created pet and verify the details are correct
		
		String cn = res.jsonPath().get("category.name");
		Map m = pet.getCategory();
		Assert.assertEquals(cn, m.get("name"));
		
		
		System.out.println("created pet's name "+pet.getName()+" category name "+m.get("name"));
			
	}
	
}
