package Base;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import Objects.Pets;
import io.restassured.RestAssured;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BaseTest {
	
	protected  Pets pet = null;
	protected  String assessToken;
	WebDriver  htmldriver = null;
	
	@BeforeClass (alwaysRun=true)
	
	public void putData () {
		
		
		int id = ids();
		String str = getStrs(6);
		pet = new Pets();
		
		pet.setId(id);
		// put the data into pet object
		HashMap category = new HashMap();
		category.put("id",Integer.toString(id));
		category.put("name", str+"_category");
		pet.setCategory(category);
		
		pet.setName("name_"+str);
		
		ArrayList<String> photoUrls = new ArrayList<String>();
		photoUrls.add("link "+str);
		pet.setPhotoUrls(photoUrls);
		
		HashMap tag = new HashMap();
		tag.put("id", Integer.toString(id));
		tag.put("name", str+"_tag");
		pet.setTag(tag);
		
		pet.setStatus("available");
	    
		RestAssured.baseURI = "https://petstore.swagger.io/v2/";
		//get Access Token by front page
	    assessToken = getAccessTokenByPage();
	    
		System.out.println(assessToken);
		
		htmldriver.close();
	}
	
	public int ids  () {
		
		Random rand = new Random();
		
		int id = rand.nextInt(1000); 
      
        return id;
	}
	
	
	public String getStrs(int n) 
    { 
  
       
        String numericString = "0123456789"+"abcdefghijklmnopqrstuvxyz"; 
  
       
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            int index 
                = (int)(numericString.length() 
                        * Math.random()); 
  
            sb.append(numericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    }
	
	public String getAccessTokenByPage () {
		
       System.setProperty("webdriver.chrome.driver", "./driver/chromedriver");
       
       htmldriver = new ChromeDriver();
	   String url = "https://petstore.swagger.io/oauth/authorize?"
				      + "response_type=token&client_id=test&scope=read:pets write:pets";
		
		htmldriver.get(url);

		String allow = "//button[@id='allow']/span";
		String comfirm = "//button[@name='login']/span";
		String autho ="//input[@name='authorize']";
		
		htmldriver.findElement(By.xpath(allow)).click();	
		waitToClick(10,autho);
		htmldriver.findElement(By.xpath(comfirm)).click();
		waitToClick(10,autho);
        htmldriver.findElement(By.xpath(autho)).click();
	
		//Get access token
        Pattern p = Pattern.compile("(?<=access_token=).+?(?=&)");
		Matcher m = p.matcher(htmldriver.getCurrentUrl());
		if (m.find()) {System.out.println("access token is "+m.group());}
		
		return  m.group();
	}
	
	public void waitToClick(int ses,String xpath) {
		
		try {
			WebElement myDynamicElement = (new WebDriverWait(htmldriver, ses))
					  .until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		}catch(Exception error) {
			htmldriver.manage().timeouts().implicitlyWait(ses, TimeUnit.SECONDS);
		}
		
	}
	
	@AfterClass(alwaysRun=true)
	public void teardDown () {
		
		 System.out.println("Test completed!");
		
		 htmldriver.quit();
	}
	

}
